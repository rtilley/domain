package domain_test

import "testing"
import "os"
import "log"
import "bufio"
import rpzdomain "code.vt.edu/rtilley/domain"

// Falsehoods
func TestBigListOfNaugthyStrings(t *testing.T) {

	tlds := rpzdomain.LoadTLDs("misc/tlds.txt")

	file, err := os.Open("misc/blns.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		naughtyString := scanner.Text()
		if rpzdomain.ValidDomain(naughtyString) && rpzdomain.ValidTLD(naughtyString, tlds) != false {
			t.Errorf("The string '%s' should be invalid.", naughtyString)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func TestNoDot(t *testing.T) {
	domainName := "com"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("the string '%s' is an invalid domain name.", domainName)
	}
}

func TestEmptyLabel(t *testing.T) {
	domainName := "whoops..com"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("the string '%s' is an invalid domain name.", domainName)
	}
}

func TestLabelTooLong(t *testing.T) {
	domainName := "label.too.long.1234567890123456789012345678901234567890123456789012345678901234.com"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("'%s' label too long. invalid domain name", domainName)
	}
}

func TestInvalidChar(t *testing.T) {
	domainName := "bad@#.com"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("'%s' invalid characters. invalid domain name", domainName)
	}
}

func TestInvalidPrefix(t *testing.T) {
	domainName := ".com"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("'%s' is an invalid domain name", domainName)
	}
}

func TestInvalidPrefix2(t *testing.T) {
	domainName := ".edu"
	if rpzdomain.ValidDomain(domainName) != false {
		t.Errorf("'%s' is an invalid domain name", domainName)
	}
}

func TestInValidTLD(t *testing.T) {
	tlds := rpzdomain.LoadTLDs("misc/tlds.txt")
	domainName := "0.00"
	if rpzdomain.ValidTLD(domainName, tlds) != false {
		t.Errorf("'%s' has an invalid tld.", domainName)
	}
}

// Truths
func TestBad(t *testing.T) {

	tlds := rpzdomain.LoadTLDs("misc/tlds.txt")

	file, err := os.Open("misc/bad.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		domainName := scanner.Text()
		if rpzdomain.ValidDomain(domainName) && rpzdomain.ValidTLD(domainName, tlds) != true {
			t.Errorf("The string '%s' failed validation.", domainName)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func TestLabelMaxSize(t *testing.T) {
	domainName := "label.max.size.123456789012345678901234567890123456789012345678901234567890123.com"
	if rpzdomain.ValidDomain(domainName) != true {
		t.Errorf("'%s' max label size (63). valid domain name", domainName)
	}
}

func TestLotsOfLabels(t *testing.T) {
	domainName := "lots.of.labels.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.1.2.3.4.5.6.7.8.9.0.com"
	if rpzdomain.ValidDomain(domainName) != true {
		t.Errorf("'%s' lots of labels. valid domain name", domainName)
	}
}

func TestValidTLD(t *testing.T) {
	tlds := rpzdomain.LoadTLDs("misc/tlds.txt")
	domainName := "valid.com"
	if rpzdomain.ValidTLD(domainName, tlds) != true {
		t.Errorf("'%s' has a valid tld.", domainName)
	}
}

// Comparisons

func TestReduceDomain1(t *testing.T) {
	if rpzdomain.ReduceDomain("valid.com", ".bl.rpz.xxx.xx.com") != "valid.com" {
		t.Error("the string 'valid.com' should be unaltered during reduction")
	}
}

func TestReduceDomain2(t *testing.T) {
	origin := ".bl.rpz.xxx.xx.com"

	too_long := "this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin"

	result := "when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin." +
		"this.domain.is.too.long.when.combined.with.origin"

	reduced := rpzdomain.ReduceDomain(too_long, origin)

	final := reduced + origin

	if reduced != result {
		t.Error("the domain should be reduced to:", reduced)
	}

	if (reduced + origin) != final {
		t.Error("the final result should be:", final)
	}
}
