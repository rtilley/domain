// Package domain provides data structures and functions
// for working with domain names in DNS RPZ based systems.
package domain

import (
	"bufio"
	"log"
	"os"
	"strings"
	"time"
)

// Domain is a structure to hold a rpz domain entity.
type Domain struct {
	// json package requires fields start with a capital letter
	Name   string    `json:"name,omitempty"`   // The domain name "bad.ru"
	Date   time.Time `json:"date,omitempty"`   // The date the domain was added
	Epoch  int64     `json:"epoch,omitempty"`  // The epoch the domain was added
	Source string    `json:"source,omitempty"` // The source or person who added it
	Reason string    `json:"reason,omitempty"` // Optional reason field
}

// Domains is a slice of Domain structures.
var Domains []Domain

// OkayChars is a string of ASCII chars that are acceptable as part of a domain name.
var OkayChars = "abcdefghijklmnopqrstuvwxyz0123456789.-_"

// ValidTLD ensures a domain ends with a valid Top Level Domain (.com, .net, .cc, etc.).
// It takes a domain name (example.com) and a slice of TLDs (.com, .net, .cc) as argmuents
// and returns true or false.
func ValidTLD(domain string, tlds []string) bool {
	cleanDomain := strings.ToLower(strings.TrimSpace(domain))
	for _, tld := range tlds {
		if strings.HasSuffix(cleanDomain, tld) {
			return true
		}
	}
	return false
}

// LoadTLDs takes a file with a list of TLDS from IANA, normalizes them and returns them as a slice.
// The data file is updated periodically and has this format:
// # Version 2019012900, Last Updated Tue Jan 29 07:07:01 2019 UTC
// AAA
// AARP
// ...
// ZW
func LoadTLDs(fileName string) []string {
	tlds := make([]string, 0)

	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		tld := strings.ToLower(strings.TrimSpace(scanner.Text()))
		if !strings.HasPrefix(tld, "#") {
			tlds = append(tlds, "."+tld)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	//log.Printf("Loaded %d TLDs.", len(tlds))

	return tlds
}

// ValidDomain ensures a domain is valid in the context of a DNS CNAME resource record.
func ValidDomain(domain string) bool {
	cleanDomain := strings.ToLower(strings.TrimSpace(domain))

	if len(cleanDomain) < 4 || len(cleanDomain) > 253 {
		return false
	}

	if strings.HasPrefix(cleanDomain, "-") || strings.HasPrefix(cleanDomain, "_") || strings.HasPrefix(cleanDomain, ".") {
		return false
	}

	if strings.HasSuffix(cleanDomain, "-") || strings.HasSuffix(cleanDomain, "_") || strings.HasSuffix(cleanDomain, ".") {
		return false
	}

	if !strings.ContainsAny(cleanDomain, ".") {
		return false
	}

	for _, r := range cleanDomain {
		if !strings.Contains(OkayChars, string(r)) {
			return false
		}
	}

	labels := strings.Split(cleanDomain, ".")
	for _, label := range labels {
		if len(label) > 63 || len(label) < 1 {
			return false
		}
	}

	return true
}

// ReduceDomain removes labels from domains until the domain + origin are less than 251 characters.
func ReduceDomain(domain string, badZone string) string {
	newDomain := domain
	totalSize := len(newDomain) + len(badZone)

	for totalSize > 251 {
		labels := strings.Split(newDomain, ".")
		newDomain = ""

		for _, label := range labels[1:] {
			newDomain += label + "."
		}

		totalSize = len(newDomain) + len(badZone)
	}

	return strings.Trim(newDomain, ".")
}
