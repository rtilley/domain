// A small helper program to retrieve the list of Top Level Domains periodically.
// usage: go run getTLDS.go
package main

import (
	"errors"
	"io"
	"log"
	"net/http"
	"os"
)

// downloadTLDS returns an error on failure, nil otherwise.
func downloadTLDS(filepath string, url string) error {

	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("HTTP Status is not 200")
	}

	_, err = io.Copy(out, resp.Body)
	return err
}

func main() {
	// https://www.icann.org/resources/pages/tlds-2012-02-25-en
	url := "http://data.iana.org/TLD/tlds-alpha-by-domain.txt"

	err := downloadTLDS("tlds.txt", url)
	if err != nil {
		log.Fatal(err)
	}
}
