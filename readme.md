# domain

A small Go package to verify and shorten domain names added to DNS RPZ zones. The context of the names and verification is limited to the CNAME resource record type.

## install the package

```bash
go get -u code.vt.edu/rtilley/domain
```

## test the package

```bash
go test -v
```

## use in your program

```bash
import "code.vt.edu/rtilley/domain"

// Optionally, do this for name conflicts
import rpzdomain "code.vt.edu/rtilley/domain"
```
